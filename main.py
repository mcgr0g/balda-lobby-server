# coding: utf-8
import urllib
import urlparse
import json
import user
import parser
import game


def createParser(path, query):
	params = urlparse.parse_qs(query)

	paths = path.split("/")
	mainPath = paths[1]
	relativePath = paths[2]

	print "mainPath = " + mainPath + "\n"
	print "relativePath = " + relativePath + "\n"

	if mainPath == "user":
		return user.User(relativePath, params)
	if mainPath == "game":
		return game.Game(relativePath, params)

def application(env, start_response):
	start_response('200 OK', [('Content-Type','text/html; charset=utf-8')])

	PATH_INFO = env["PATH_INFO"]
	QUERY_STRING = env["QUERY_STRING"]

	parser_ = createParser(PATH_INFO, QUERY_STRING)
	response = parser_.parse()

	print "response is", response
	return json.dumps(response)
