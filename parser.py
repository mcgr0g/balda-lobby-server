class Parser:
	def __init__(self, path, params):
		self.path = path
		self.params = params

	def checkParams(self, param_to_check):
		for param in self.params:
			if param == param_to_check:
				return True, self.params[param_to_check][0]
		error = {"type": "error", "code": 100, "fatal": True, "message": "Bad path"}
		return False, error

	def parse(self):
		raise NotImplementedError('Method Parser.parse is pure virtual')