import parser, database


class User(parser.Parser):
	def parse(self):
		if self.path == "signup":
			return self.parseSignUp()
		if self.path == "signin":
			return self.parseSignIn()

		return "Bad path: %s " % self.path

	def loginExists(self, login, cur):
		cur.execute("""SELECT `login` FROM `users` WHERE `login` = '%s'""" % (login,))
		result = cur.fetchall()
		if len(result) == 0:
			return False
		else:
			return True

	def parseSignUp(self):
		exists, login = self.checkParams("login")
		if not exists:
			return login

		exists, passwd = self.checkParams("pass")
		if not exists:
			return passwd

		exists, inputToken = self.checkParams("token")
		if not exists:
			return inputToken

		generatedToken = login + passwd
		db, cur = database.connect()
		if self.loginExists(login, cur):
			db.close()
			return "Login already used"
		if inputToken == generatedToken:
			cur.execute('INSERT into `users` (`login`, `pass`, `token`) VALUES (%s,%s,%s)', (login, passwd, inputToken))
			db.commit()
			db.close()
			return {"token": inputToken}
		else:
			db.close()
			return {"type": "error", "code": 100, "fatal": True, "message": "Bad Token"}

	def parseSignIn(self):
		exists, login = self.checkParams("login")
		if not exists:
			return login

		exists, passwd = self.checkParams("pass")
		if not exists:
			return passwd
		db, cur = database.connect()
		cur.execute("""SELECT `token` FROM `users` WHERE `login` = "%s" AND `pass` = '%s'""" % (login, passwd))
		token = cur.fetchall()
		db.close()
		if len(token) == 0:
			return {"type": "error", "code": 100, "fatal": True, "message": "There is wrong login or pass"}
		else:
			return {"token": token[0][0]}
