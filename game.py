import re, urllib, httplib, json
import parser, database, settings


class Game(parser.Parser):
	workerUrl = settings.worker_host + ":" + settings.worker_port

	def parse(self):
		if self.path == "start_game":
			return self.start_game()
		if self.path == "create":
			return self.create()

		return "Bad path: %s " % self.path

	def start_game(self):
		exists, inputToken = self.checkParams("token")
		if not exists:
			return inputToken
		if re.match('^\w*$', inputToken) is None:
			return "invalid token"
		token = inputToken

		open_sessions = self.search_open_sessions(token)["sessions"]

		if not open_sessions:
			session_to_play = self.create_session(token)
		else:
			# if there will be friendlist in game we need to make select for friends in open_sessions
			session_to_play = open_sessions[0]
			self.mark_session_as_used(session_to_play, token)

		# print session_to_play
		succses, result = self.attach_player(session_to_play, token)
		if not succses:
			# there will be error handler -- todo
			if result == "Session full":
				return {"error": result}
			elif result == "Session not found":
				return {"error": result}
		else:
			pid, sid = result[0], result[1]
			return {"type": "hello", "pid": pid, "sid": sid}

	def search_open_sessions(self, token):
		db, cur = database.connect()
		# deleting old games of current user form db
		cur.execute("""delete from `games` where `user1` = "%s" or `user2` = '%s' """ % (token, token))
		db.commit()
		# searching for waiting players
		cur.execute("""select `session_id` from `games` where `user2` is null""")
		session_id = cur.fetchall()
		db.close()
		sessions = []
		for session in session_id:
			sessions.append(session[0])
		return {"sessions": sessions}

	def mark_session_as_used(self, session_to_mark, token):
		db, cur = database.connect()
		# this is for debug. After all need to be commented
		cur.execute("""UPDATE `games` SET `user2`='%s' WHERE `session_id`='%s'""" % (token, session_to_mark))
		# and this should work
		# cur.execute("""delete from `games` WHERE `session_id`='%s'""" % (session_toplay,))
		db.commit()
		db.close()

	def create_session(self, token):
		"""
		Function get new session id from game server, put it to the db and returns id as string
		"""
		selector = "/create_session"
		worker = httplib.HTTPConnection(self.workerUrl)
		worker.request("GET", selector)
		res = worker.getresponse()
		data = res.read()
		dict = json.loads(data)
		session = dict['session_id']
		db, cur = database.connect()
		cur.execute("""insert into `games` values (default, "%s", default, "%s" )""" % (token, session))
		db.commit()
		db.close()
		return session # type = string

	def attach_player(self, session_id, token):
		"""
		Function give session if to game server, get player id. If something gone bad - there will be message.
		"""
		selector = "/attach_player"
		headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
		params = urllib.urlencode({"session_id": session_id})

		worker = httplib.HTTPConnection(self.workerUrl)
		worker.request("POST", selector, params, headers)
		response = worker.getresponse()
		data = response.read()
		worker.close()
		dict = json.loads(data)
		if dict['type'] == "response":
			return True, (dict['player_id'], dict['session_id'])
		else:
			return False, dict['message']

	def create(self):
		"""
		Test function to make valid entries in db
		"""
		exists, inputToken = self.checkParams("token")
		if not exists:
			inputToken
		selector = "/create_session"

		worker = httplib.HTTPConnection(self.workerUrl)
		worker.request("GET", selector)
		res = worker.getresponse()
		data = res.read()
		dict = json.loads(data)
		session = dict['session_id']
		db, cur = database.connect()
		cur.execute("""insert into `games` values (default, "%s", default, "%s" )""" % (inputToken, session))
		db.commit()
		db.close()
		return session # type = string