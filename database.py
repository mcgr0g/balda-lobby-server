import MySQLdb, settings

host = settings.database_host
user = settings.database_user
passwd = settings.database_pass
base = settings.database_db

def connect():
	db = MySQLdb.connect(host=host, user=user, passwd=passwd, db=base)
	cur = db.cursor()
	return db, cur